#include "gestion_entites.h"

void creer_entite(SDL_Window *window, SDL_Renderer *renderer, int pos_prec, int pos_cour, int vitesse, int vertical, int horizontal, entite_t **pentite, char *chemin)
{
    *pentite = (entite_t *)malloc(sizeof(entite_t));
    if (*pentite)
    {
        (*pentite)->pos_prec = pos_prec;
        (*pentite)->pos_cour = pos_cour;
        (*pentite)->vitesse = vitesse;
        (*pentite)->vertical = vertical;
        (*pentite)->horizontal = horizontal;
        charger_texture(window, renderer, *pentite, chemin);
    }
}

void charger_texture(SDL_Window *window, SDL_Renderer *renderer, entite_t *entite, char *chemin)
{
    SDL_Texture *my_texture = NULL;
    my_texture = IMG_LoadTexture(renderer, chemin);
    if (my_texture == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture", window, renderer);
    entite->texture = my_texture;
}

void liberer_entite(entite_t *entite)
{
    SDL_DestroyTexture(entite->texture);
    free(entite);
    entite = NULL;
}

void affichage_entite(SDL_Window *window, SDL_Renderer *renderer, entite_t *entite, int *delta, float anim)
{
    float taille = 0.01;
    int idle = 0;

    SDL_Rect
        source = {0},            // Rectangle définissant la zone totale de la planche
        window_dimensions = {0}, // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0},       // Rectangle définissant où la zone_source doit être déposée dans le renderer
        state = {0};             // Rectangle de la vignette en cours dans la planche

    SDL_GetWindowSize(window, // Récupération des dimensions de la fenêtre
                      &window_dimensions.w,
                      &window_dimensions.h);
    SDL_QueryTexture(entite->texture, // Récupération des dimensions de l'image
                     NULL, NULL,
                     &source.w, &source.h);
    /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

    int nb_images = 4;                   // Il y a 8 vignette dans la ligne de l'image qui nous intéresse
    int offset_x = source.w / nb_images, // La largeur d'une vignette de l'image, marche car la planche est bien réglée
        offset_y = source.h / 4;         // La hauteur d'une vignette de l'image, marche car la planche est bien réglée

    //float zoom = LARGEUR_CASE/offset_x;                   // zoom, car ces images sont un peu petites

    state.x = 0;
    state.w = offset_x;
    state.h = offset_y;
    if (entite->horizontal == 0 && entite->vertical == 0)
        idle = 1;
    else
        idle = 0;

    if (entite->vertical == 1)
        state.y = 2 * offset_y;
    else if (entite->vertical == -1)
        state.y = 3 * offset_y;

    if (entite->horizontal == 1)
        state.y = 0 * offset_y;
    else if (entite->horizontal == -1)
        state.y = 1 * offset_y;

    state.x = ((int)anim % 4) * offset_x;

    float zoom = 30 ;    // zoom, car ces images sont un peu petites
    destination.w =  zoom; // Largeur du sprite à l'écran
    destination.h =  zoom; // Hauteur du sprite à l'écran

    //destination.y = 0.1 * window_dimensions.h; // La course se fait en milieu d'écran (en vertical)

    if (!idle)
    {
        state.x += offset_x;
        state.x %= source.w;
    }
    else
    {
        state.y = 0;
        state.x = 0;
    }
    int i = entite->pos_prec / NB_COLONNE_LABY;
    int j = entite->pos_prec % NB_COLONNE_LABY;
    float val = 0.070;

    destination.y = i * HAUTEUR_CASE - 1 + (*delta * val) * entite->horizontal;
    destination.x = j * LARGEUR_CASE + 5  + (*delta * val) * entite->vertical;

    /* * LARGEUR_CASE + (*deltaTimeNB_COLONNE_LABY * *input_h * TRANSI);*/
    //destination.y += entite->vitesse;
    // printf("x : %d y : %d\n", destination.x, destination.y);

    SDL_RenderCopy(renderer, entite->texture, &state, &destination);
}

//retourne 0 si pas de collision
int collision(entite_t *perso, entite_t *liste_boule[NB_BOULES], bombe_t *bombe, int *nb_bombes, int map[NB_LIGNE_LABY][NB_COLONNE_LABY])
{
    int res = 0,erreur;
    for (int k = 0; k < NB_BOULES; ++k)
    {
        if (perso->pos_cour == liste_boule[k]->pos_cour && res == 0)
        {
            res = 1;
        }
    }
    if (*nb_bombes > 0)
    {
        if (SDL_GetTicks() > bombe->pose_bombe + bombe->temps)
        {
            erreur = explosion(bombe,perso,liste_boule,map); //bombe liberee
            printf("bombe explose ! \n");
            (*nb_bombes)--;
            if (erreur < 0) //perso explose
            {
                res = 1;
            }
        }
    }
    return res;
}
